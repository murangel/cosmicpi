#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

const int NUM_ARQUIVOS = 3;
const int NUM_COLUNAS = 15;

int main() {
  // Vetor de vetores para armazenar os dados dos arquivos
  std::vector<std::vector<std::vector<double>>> dados(NUM_ARQUIVOS);

  // Nomes dos arquivos
  std::vector<std::string> nomes_arquivos = {"det1.txt", "det2.txt", "det3.txt"};

  for (int i = 0; i < NUM_ARQUIVOS; ++i) {
    std::ifstream arquivo(nomes_arquivos[i]);

    if (!arquivo.is_open()) {
      std::cerr << "Erro ao abrir o arquivo " << nomes_arquivos[i] << std::endl;
      return 1;  // Encerrar o programa com código de erro
    }

    std::string linha;
    while (std::getline(arquivo, linha)) {
      std::istringstream ss(linha);
      std::vector<double> colunas(NUM_COLUNAS);

      for (int coluna = 0; coluna < NUM_COLUNAS; ++coluna) {
        std::string valor_str;
        if (std::getline(ss, valor_str, ',')) {
          colunas[coluna] = std::stod(valor_str);
        } else {
          std::cerr << "Erro ao ler dados do arquivo " << nomes_arquivos[i] << std::endl;
          return 1;  // Encerrar o programa com código de erro
        }
      }

      dados[i].push_back(colunas);
    }

    arquivo.close();
  }

  int n123 = 0; //conicidencia entre det1, det2 e det3
  int n13 = 0; //coincidencia entre det1 e det3


  for (const auto& linha_det1 : dados[0]) 
  for (const auto& linha_det2 : dados[1]) 
  for (const auto& linha_det3 : dados[2]) 
    if(linha_det1[0] == linha_det2[0] && linha_det2[0] == linha_det3[0]) n123++;
  
  for (const auto& linha_det1 : dados[0]) 
  for (const auto& linha_det3 : dados[2]) 
    if(linha_det1[0] == linha_det3[0]) n13++;

  std::cout << "LAPE n123 = " << n123 << " n13 = " << n13 << " eff = " << (float)n123/n13 << std::endl;

  // Exibir os dados lidos (apenas para fins de verificação)
  for (int i = 0; i < NUM_ARQUIVOS; ++i) {
    std::cout << "Dados do arquivo " << nomes_arquivos[i] << ":\n";
    for (const auto& linha : dados[i]) {
      for (const auto& valor : linha) {
        std::cout << valor << " ";
      }
      std::cout << "\n";
    }
    std::cout << "\n";
  }

  return 0;
}
